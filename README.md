# React Native Spotify App

This is a react-native implementation of a Spotify app that can search for artist, list albums and play some music ;) 

### Install all required dependencies
`npm install`


### [Mac users only]
`cd ios && pod install && cd ..`


### Get your credentials from Spotify

Go to [Spotify Web Api]([https://developer.spotify.com/documentation/web-api/quick-start/]) and follow the instructions under "Set Up Your Account" section.  Create new client ID from your Spotify dashboard.

### Configuration
Put your Spotify client ID & client secret in settings.js


### Run
`react-native run-ios / react-native run-android`


### Lint

It's important that you've linted your code before you can contribute to this repo.

`npm run lint` should give no errors or warnings.
