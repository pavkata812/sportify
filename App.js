import 'react-native-gesture-handler';
import React from 'react';
import { SafeAreaView, StatusBar, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-redux';
import store from './src/redux/store';


import { Home, Artists, Albums } from './src/screens';

const Stack = createStackNavigator();

function App() {
  return (
      <Provider store={store}>
        <StatusBar />
        <NavigationContainer>
          <SafeAreaView style={styles.safeArea}>
              <Stack.Navigator>
                <Stack.Screen name="Home" component={Home}  options={{ headerShown: false }} />
                <Stack.Screen name="Artist" component={Artists} options={({ route }) => ({ title: route.params.artist.name })} />
                <Stack.Screen name="Album" component={Albums} />
              </Stack.Navigator>
          </SafeAreaView>
        </NavigationContainer>
      </Provider>
  );
}

export default App;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
});
