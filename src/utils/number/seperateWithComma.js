export default function seperateWithComma (str) {
  	// Format number in thousand separator format. 1234567 becomes 1,234,567
  	return str.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}