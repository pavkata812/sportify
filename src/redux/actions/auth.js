export const AUTH = 'AUTH';

export function doAuth(token) {
  return { type: AUTH, token };
}
