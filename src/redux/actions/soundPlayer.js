export const SET_TRACK = 'SET_TRACK';

export function setTrack(track) {
  return { type: SET_TRACK, track };
}
