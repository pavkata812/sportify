import { combineReducers } from 'redux'
import authReducer from "./auth"
import soundPlayerReducer from "./soundPlayer"

export default combineReducers({
	auth: authReducer,
	soundPlayer: soundPlayerReducer
});