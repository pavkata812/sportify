const authReducer = (state = {
    token: '',
}, action) => {
    switch (action.type) {
        case 'AUTH':
            return { ...state, token: action.token };
        default:
            return state;
    }
};
export default authReducer;