const soundPlayerReducer = (state = {
    currentTrack: '',
}, action) => {
    switch (action.type) {
        case 'SET_TRACK':
            return { ...state, currentTrack: action.track };
        default:
            return state;
    }
};
export default soundPlayerReducer;