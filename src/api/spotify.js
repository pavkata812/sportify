import { spotifyClientId, spotifyClientSecret } from '../config';
import { Buffer } from 'buffer';

const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': 'Bearer ',
};
const api_endpoint = 'https://api.spotify.com/v1/';
const auth_endpoint = 'https://accounts.spotify.com/api/token';

const base64credentials = new Buffer(spotifyClientId + ':' + spotifyClientSecret).toString('base64');


export async function authenticatedFetch(request, params) {
    try {
      let response = await fetch(auth_endpoint, {
        method: 'POST',
        headers: {
          'Authorization': `Basic ${base64credentials}`,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'grant_type=client_credentials',
     });

      let responseJson = await response.json();
      let args = [responseJson.access_token, ...params];
      let result = await request.apply(this, args);
      return result;
    } catch (error){}
}

export async function spotifySearch(token, query) {
    try {
     let response = await fetch(api_endpoint + 'search?type=artist&q=' + query + '&limit=50' , {
        method: 'GET',
        headers: Object.assign(headers, {'Authorization': 'Bearer ' + token}),
        });
      let responseJson = await response.json();
      return responseJson.artists;
    } catch (error){}
}

export async function spotifyAlbums(token, artistId) {
    try {
      let response = await fetch(api_endpoint + 'artists/' + artistId + '/albums?limit=50', {
        method: 'GET',
        headers: Object.assign(headers, {'Authorization': 'Bearer ' + token}),
        });
      let responseJson = await response.json();
      return responseJson;
    } catch (error){}
}

export async function spotifyTracks(token, albumId) {
    try {
      let response = await fetch(api_endpoint + 'albums/' + albumId + '/tracks', {
        method: 'GET',
        headers: Object.assign(headers, {'Authorization': 'Bearer ' + token}),
        });
      let responseJson = await response.json();
      return responseJson;
    } catch (error){}
}
