import React, { useState, useEffect } from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import SoundPlayer from 'react-native-sound-player';
import { spotifyTracks, authenticatedFetch } from '../api/spotify';
import { setTrack } from '../redux/actions/soundPlayer';

import Track from '../components/Track';

import playImage from '../../public/images/play.png';
import stopImage from '../../public/images/stop.png';

export function Albums(props) {
  const dispatch = useDispatch();
  const [items, setItems] = useState([]);
  const album = props.route.params.album;
  const currentTrack = useSelector(state => state.soundPlayer.currentTrack);
  let _onFinishedPlayingSubscription = null;

  useEffect(() => {
    _onFinishedPlayingSubscription = SoundPlayer.addEventListener('FinishedPlaying', ({ success }) => {
      dispatch(setTrack(null));
    })
    return function cleanup() {
      dispatch(setTrack(null));
      SoundPlayer.stop();
      SoundPlayer.unmount();
      _onFinishedPlayingSubscription.remove()
    };
  }, [])

  useEffect(() => {
    authenticatedFetch(spotifyTracks, [album.id]).then(response => {
        setItems(response.items);
    });

  },[album]);

  useEffect(() => {
    let track = items[currentTrack] || {};
    SoundPlayer.stop();
    if (track.preview_url) {
      // dispatch(setTrack(track.id));
      SoundPlayer.playUrl(track.preview_url);
    }
  }, [currentTrack])

  let image = (album.images && album.images.length > 0) ? album.images[0].url : null;
  return (
    <View style={styles.view}>
        <View style={styles.listHeader}>
           {image && <Image style={styles.albumImage} source={{uri: image}} />}
          <View style={styles.listHeaderText}>
            <Text style={styles.albumName}>{album.name}</Text>
            <Text style={styles.albumReleased}>Released on {album.release_date}</Text>
            <Text style={styles.albumTracks}>{album.total_tracks} tracks</Text>
          </View>
      </View>

      <ScrollView>
        {
          items.map((item, key) => <Track item={item} key={key} trackKey={key} />)
        }
        </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    paddingBottom: 10,
    flex: 1,
  },
  album : {
    flexDirection: 'row',
    padding: 15,
  },
  listHeader: {
    alignItems: 'center',
    flexDirection: 'row',
    padding: 20,
    marginBottom: 20,
    borderBottomColor: '#e1e1e1',
    borderBottomWidth: 1,
    backgroundColor: '#f3f3f3',
  },
  listHeaderText: {
    marginLeft: 20,
    flex: 1,
  },
  albumName: {
    fontSize: 22,
    fontWeight: 'bold',
  },
  albumReleased: {
    fontSize: 16,
    marginVertical: 5,
  },
  albumTracks: {
    fontSize: 14,
    color: '#666666',
  },
  albumImage: {
    width: 140,
    height: 140,
  },
   image: {
    width: 96,
    height: 96,
  },
});
