import React, { useState, useEffect, useRef } from 'react';
import { TextInput, View, Text, Image, ScrollView, StyleSheet, TouchableOpacity, Alert, Animated } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { authenticate, spotifySearch, authenticatedFetch } from '../api/spotify';
import { doAuth } from '../redux/actions/auth';

import Artist from '../components/Artist';

export function Home({ navigation }) {
  const [value, setValue] = useState('');
  const [searchPosition, setSearchPosition] = useState(0);
  const [artists, setArtists] = useState([]);
  const [total, setTotal] = useState('');
  const dispatch = useDispatch();
  const minSearchLength = 2;

  const translateY = useRef(new Animated.Value(0)).current;

  const slideHeroUp = () => {
    Animated.timing(translateY, {
      toValue: -searchPosition,
      duration: 400,
      useNativeDriver: true,
    }).start();
  };


  useEffect(() => {
    if (value && value.length > minSearchLength - 1) {
        authenticatedFetch(spotifySearch, [value]).then(response => {
            setTotal(response.total > 50 ? '50+' : response.total);
            setArtists(response.items);
        });
    } else {
        setArtists([]);
        setTotal('');
    }
  }, [value]);

  return (
    <Animated.View style={[styles.view, {transform: [{ translateY: translateY }]}]}>
    <View style={styles.hero}>
        <Image style={styles.heroImage} source={require('../../public/images/logo.png')}/>
        <Text style={styles.heroTitle}>Spotify Project</Text>
        <Text style={styles.heroSummary}>Search for your favourite artist and listen songs right in the app!</Text>
      </View>
      <TextInput
        style={styles.searchInput}
        placeholder={'Search for artist'}
        onChangeText={text => setValue(text.replace(/[^0-9a-zA-Z\s]/g, ''))}
        onFocus={slideHeroUp}
        value={value}
        onLayout={({nativeEvent}) => {
          setSearchPosition(nativeEvent.layout.y);
        }}
      />
    <ScrollView>
      {
        (total != '' ? <Text style={styles.listHeader}>Artists found : {total}</Text> : null)
      }
      {
        artists.map((item, key) => <Artist item={item} key={key} />)
      }
      </ScrollView>
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  view: {
    paddingBottom: 40,
  },
  hero: {
    padding: 50,
    alignItems: 'center',
  },

  heroImage: {
    width: 180,
    height: 180,
    borderRadius: 90,
  },
  heroTitle: {
    fontSize: 36,
    marginTop: 20,
    textAlign: 'center',
  },
  heroSummary: {
    fontSize: 18,
    marginTop: 20,
    textAlign: 'center',
    color: '#666666',
    lineHeight: 28,
  },
  listHeader: {
    fontSize: 16,
    textAlign: 'center',
    padding: 20,
    color: '#666',
  },
  searchInput: {
    fontSize: 20,
    padding: 15,
    marginHorizontal: 30,
    borderBottomColor: '#e1e1e1',
    borderBottomWidth: 1,
  },
});
