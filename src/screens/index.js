import {Home} from './Home';
import {Artists} from './Artists';
import {Albums} from './Albums';

export {
    Home,
    Artists,
    Albums,
};
