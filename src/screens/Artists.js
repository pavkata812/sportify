import React, { useState, useEffect, useRef } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Animated } from 'react-native';
import { useSelector } from 'react-redux';
import { spotifyAlbums, authenticatedFetch } from '../api/spotify';
import seperateWithComma from "../utils/number/seperateWithComma";

import Album from '../components/Album';

export function Artists(props) {
  const [albums, setAlbums] = useState([]);
  const artist = props.route.params.artist;
  const interpolation = {inputRange: [0, 100, 200], outputRange: [180, 180, 60],extrapolate: 'clamp'};
  const interpolationFontSize = {inputRange: [0, 100, 200], outputRange: [36, 36, 20],extrapolate: 'clamp'};


  const scaleDimension = useRef(new Animated.Value(1)).current;

  useEffect(() => {
        authenticatedFetch(spotifyAlbums, [artist.id]).then(response => {
            setAlbums(response.items);
        });
  },[artist]);

  let image = (artist.images && artist.images.length > 0) ? artist.images[0].url : null;

  let followers = seperateWithComma(artist.followers.total);

  return (
    <View style={styles.view}>
        <View style={styles.listHeader}>
           {image && <Animated.Image
              style={[styles.artistImage, { height: scaleDimension.interpolate(interpolation),width: scaleDimension.interpolate(interpolation)}]}
              source={{uri: image}}
            />}
          <View>
            <Animated.Text style={[styles.listHeaderText, { fontSize: scaleDimension.interpolate(interpolationFontSize)}]}>{artist.name} </Animated.Text>
            <Text style={styles.followers}>{followers} followers</Text>
          </View>
      </View>

       <Animated.ScrollView
          scrollEventThrottle={1}
          onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: scaleDimension } } }], { useNativeDriver: false })} >
        {
          albums.map((item, key) => <Album item={item} key={key} />)
        }
        </Animated.ScrollView>
    </View>
  );
}


const styles = StyleSheet.create({
  view: {
    paddingBottom: 10,
    flex: 1,
  },
  album : {
    flexDirection: 'row',
    padding: 15,
  },
  listHeader: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    paddingBottom: 20,
    marginBottom: 0,
    borderBottomColor: '#e1e1e1',
    borderBottomWidth: 1,
    backgroundColor: '#f3f3f3',
  },
  listHeaderText: {
    fontSize: 36,
    fontWeight: 'bold',
    marginTop: 10,
    textAlign: 'center',
  },
  separator: {
    height: 1,
    backgroundColor: '#ccc',
    alignSelf: 'center',
    width: '80%',
  },
  artistImage: {
    width: 180,
    height: 180,
    borderRadius: 90,
  },
  followers: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
