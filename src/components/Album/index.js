import React, { useState, useEffect, useRef } from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, Text, Image, StyleSheet, TouchableOpacity, Animated } from 'react-native';

export default function Album({item}) {
  let navigation = useNavigation();
  let albumImage = (item.images && item.images.length > 0) ? {uri: item.images[0].url} : require('../../../public/images/empty.png');
  return (
    <TouchableOpacity onPress={() => navigation.push('Album', {album: item})}>
        <View style={styles.album}>
        <Image style={styles.image} source={albumImage} />
        <View style={styles.info}>
            <Text style={styles.albumName}>{item.name}</Text>
            <Text style={styles.albumReleased}>Released on {item.release_date}</Text>
            <Text style={styles.albumTracks}>{item.total_tracks} tracks</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}


const styles = StyleSheet.create({
  album : {
    flexDirection: 'row',
    padding: 15,
  },
  image: {
    width: 96,
    height: 96,
  },
  info: {
    paddingLeft: 15,
    justifyContent: 'center',
    flex: 1,
  },
  albumName: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  albumReleased: {
    fontSize: 16,
    marginVertical: 5,
  },
  albumTracks: {
    fontSize: 14,
    color: '#666666',
  }
});
