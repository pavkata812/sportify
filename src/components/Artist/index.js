import React, { useState, useEffect, useRef } from 'react';
import { TextInput, View, Text, Image, ScrollView, StyleSheet, TouchableOpacity, Alert, Animated } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import seperateWithComma from "../../utils/number/seperateWithComma";

export default function Artist({ item }) {
	let navigation = useNavigation();
	let image = (item.images && item.images.length > 0) ? {uri: item.images[0].url} : require('../../../public/images/empty.png');
	let followers = seperateWithComma(item.followers.total);
  return (
    <TouchableOpacity onPress={() => navigation.push('Artist', {artist: item})}>
      <View style={styles.artist}>
        <Image style={styles.image} source={image}/>
	      <View style={styles.info}>
	          <Text style={styles.artistName}>{item.name}</Text>
	          <Text>{followers} followers</Text>
	      </View>
	    </View>
	  </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  artist : {
    flexDirection: 'row',
    padding: 15,
  },
  image: {
    width: 64,
    height: 64,
    borderRadius: 32,
  },
  info: {
    paddingLeft: 15,
    justifyContent: 'center',
    width: '85%',
  },
  artistName: {
    fontWeight: 'bold',
    fontSize: 20,
  }
});
