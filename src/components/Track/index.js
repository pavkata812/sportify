import React, { useState, useEffect } from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
import SoundPlayer from 'react-native-sound-player';
import { setTrack } from '../../redux/actions/soundPlayer';

import playImage from '../../../public/images/play.png';
import stopImage from '../../../public/images/stop.png';

export default function Track({item, trackKey}) {
  const dispatch = useDispatch();
  const currentTrack = useSelector(state => state.soundPlayer.currentTrack);
  const isPlaying = currentTrack === trackKey;
  return (
    <TouchableOpacity onPress={() => isPlaying ? dispatch(setTrack(null)) : dispatch(setTrack(trackKey))}>
      <View style={styles.track}>
        { item.preview_url ? <Image
          style={ styles.playControl }
          source={ isPlaying ? stopImage : playImage }
        /> : null }
        <Text style={styles.trackName}>{item.track_number}. {item.name}</Text>
        {item.explicit ? <Text style={styles.explicit}>explicit</Text> : null}
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  track: {
    marginLeft: '5%',
    width: '90%',
    paddingVertical: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  trackName: {
    fontSize: 16,
    flex: 1,
  },
  playControl: {
    width: 24,
    height: 24,
    marginRight: 5,
  },
  explicit: {
    justifyContent: 'flex-end',
    alignSelf: 'flex-start',
    fontSize: 12,
    padding: 5,
    color: '#ffffff',
    backgroundColor: '#333',
    marginLeft: 5,
  }
});
